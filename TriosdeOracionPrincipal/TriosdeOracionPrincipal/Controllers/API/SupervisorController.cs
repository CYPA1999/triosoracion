﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace TriosdeOracionPrincipal.Controllers.API
{
    public class SupervisorController : Controller
    {
        // GET: Supervisor
        public ActionResult Index()
        {
            IEnumerable<Supervisor> supervisor = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44324/api/");

                var responseTask = client.GetAsync("Supervisor");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Supervisor>>();
                    readTask.Wait();

                    supervisor = readTask.Result;
                }
                else
                {
                    supervisor = Enumerable.Empty<Supervisor>();
                    ModelState.AddModelError(string.Empty, "Error en el servidor. Por favor pongase en contacto con el administrador ");
                }
            }
            return View(supervisor);
        }
        public ActionResult Create()
        {
            return View();
        }
        [System.Web.Mvc.HttpPost]
        public ActionResult Create(Supervisor supervisor)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44324/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Supervisor>("Supervisor", supervisor);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Error en el servidor. Por favor pongase en contacto con el administrador ");

            return View(supervisor);
        }
        public ActionResult Edit(int id)
        {
            Supervisor supervisor = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44324/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Supervisor?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Supervisor>();
                    readTask.Wait();

                    supervisor = readTask.Result;
                }
            }

            return View(supervisor);
        }
        [System.Web.Mvc.HttpPost]
        public ActionResult Edit(Supervisor supervisor)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44324/api/Supervisor");

                //HTTP POST
                var putTask = client.PutAsJsonAsync<Supervisor>("Supervisor", supervisor);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return View(supervisor);
        }
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44324/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Supervisor/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    }
}