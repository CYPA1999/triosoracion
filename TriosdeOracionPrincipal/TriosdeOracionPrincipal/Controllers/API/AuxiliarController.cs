﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace TriosdeOracionPrincipal.Controllers.API
{
    public class AuxiliarController : Controller
    {
        // GET: Auxiliar
        public ActionResult Index()
        {
            IEnumerable<Auxiliar> auxiliar = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44324/api/");

                var responseTask = client.GetAsync("Auxiliar");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Auxiliar>>();
                    readTask.Wait();

                    auxiliar = readTask.Result;
                }
                else
                {
                    auxiliar = Enumerable.Empty<Auxiliar>();
                    ModelState.AddModelError(string.Empty, "Error en el servidor. Por favor pongase en contacto con el administrador ");
                }
            }
            return View(auxiliar);
        }
        public ActionResult Create()
        {
            return View();
        }
        [System.Web.Mvc.HttpPost]
        public ActionResult Create(Auxiliar auxiliar)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44324/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Auxiliar>("Auxiliar", auxiliar);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Error en el servidor. Por favor pongase en contacto con el administrador ");

            return View(auxiliar);
        }
        public ActionResult Edit(int id)
        {
            Auxiliar auxiliar = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44324/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Auxiliar?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Auxiliar>();
                    readTask.Wait();

                    auxiliar = readTask.Result;
                }
            }

            return View(auxiliar);
        }
        [System.Web.Mvc.HttpPost]
        public ActionResult Edit(Auxiliar auxiliar)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44324/api/Auxiliar");

                //HTTP POST
                var putTask = client.PutAsJsonAsync<Auxiliar>("Auxiliar", auxiliar);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return View(auxiliar);
        }
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44324/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Auxiliar/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    }
}