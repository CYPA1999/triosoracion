﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
namespace TriosdeOracionPrincipal.Controllers.API
{
    public class PastorController : Controller
    {
        // GET: Ministerio
        public ActionResult Index()
        {
            IEnumerable<Pastor> personas = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44324/api/");

                var responseTask = client.GetAsync("Pastor");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Pastor>>();
                    readTask.Wait();

                    personas = readTask.Result;
                }
                else
                {
                    personas = Enumerable.Empty<Pastor>();
                    ModelState.AddModelError(string.Empty, "Error en el servidor. Por favor pongase en contacto con el administrador ");
                }
            }
            return View(personas);
        }
        public ActionResult Create()
        {
            return View();
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Create(Pastor pastor)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44324/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Pastor>("Pastor", pastor);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Error en el servidor. Por favor pongase en contacto con el administrador ");

            return View(pastor);
        }
        public ActionResult Edit(int id)
        {
            Pastor pastor = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44324/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Pastor?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Pastor>();
                    readTask.Wait();

                    pastor = readTask.Result;
                }
            }

            return View(pastor);
        }
        [System.Web.Mvc.HttpPost]
        public ActionResult Edit(Pastor pastor)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44324/api/Pastor");

                //HTTP POST
                var putTask = client.PutAsJsonAsync<Pastor>("Pastor", pastor);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return View(pastor);
        }
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44324/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Pastor/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }

    }
}