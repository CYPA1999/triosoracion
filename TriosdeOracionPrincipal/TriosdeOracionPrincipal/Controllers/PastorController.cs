﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TriosdeOracionPrincipal.Controllers
{
    public class PastorController : ApiController
    {
        public IHttpActionResult Get()
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    IEnumerable<Pastor> pastor = db.Pastors.ToList();
                    return Ok(pastor);
                }
                catch (Exception ex)
                {
                    return NotFound();
                }
        }

        // GET api/values/5
        public IHttpActionResult Get(int id)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    Pastor pastor = db.Pastors.Find(id);
                    return Ok(pastor);
                }
                catch (Exception ex)
                {
                    return NotFound();
                }
        }

        // POST api/values
        public IHttpActionResult Post([FromBody]Pastor value)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    db.Pastors.Add(value);
                    db.SaveChanges();
                    return Ok(value);
                }
                catch (Exception ex)
                {

                    return BadRequest();
                }

        }

        // PUT api/values/5
        public IHttpActionResult Put(int id, [FromBody]Pastor value)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    Pastor pastor = db.Pastors.Where(d => d.IdPastor == id).First();
                    pastor.Nombre = value.Nombre;
                    pastor.Apellido = value.Apellido;
                    pastor.FecNac = value.FecNac;
                    pastor.EstadoCivil = value.EstadoCivil;
                    pastor.Sexo = value.Sexo;
                    db.Entry(pastor).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return Ok(pastor);
                }
                catch (Exception ex)
                {

                    return BadRequest();
                }
        }

        // DELETE api/values/5
        public IHttpActionResult Delete(int id)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    Pastor pastor = db.Pastors.Find(id);
                    db.Pastors.Remove(pastor);
                    db.SaveChanges();
                    return Ok(id);
                }
                catch (Exception ex)
                {
                    return BadRequest();
                }
        }
    }
}
