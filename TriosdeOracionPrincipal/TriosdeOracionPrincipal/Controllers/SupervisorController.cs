﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TriosdeOracionPrincipal.Controllers
{
    public class SupervisorController : ApiController
    {
        // GET: api/Supervisor
        public IHttpActionResult Get()
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    IEnumerable<Supervisor> supervisor = db.Supervisors.ToList();
                    return Ok(supervisor);
                }
                catch (Exception ex)
                {
                    return NotFound();
                }
        }

        // GET api/values/5
        public IHttpActionResult Get(int id)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    Supervisor supervisor = db.Supervisors.Find(id);
                    return Ok(supervisor);
                }
                catch (Exception ex)
                {
                    return NotFound();
                }
        }

        // POST api/values
        public IHttpActionResult Post([FromBody]Supervisor value)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    db.Supervisors.Add(value);
                    db.SaveChanges();
                    return Ok(value);
                }
                catch (Exception ex)
                {

                    return BadRequest();
                }

        }

        // PUT api/values/5
        public IHttpActionResult Put(int id, [FromBody]Supervisor value)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    Supervisor supervisor = db.Supervisors.Where(d => d.IdSupervisor == id).First();
                    supervisor.Nombre = value.Nombre;
                    supervisor.Apellido = value.Apellido;
                    supervisor.FecNac = value.FecNac;
                    supervisor.EstadoCivil = value.EstadoCivil;
                    supervisor.Sexo = value.Sexo;
                    db.Entry(supervisor).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return Ok(supervisor);
                }
                catch (Exception ex)
                {

                    return BadRequest();
                }
        }

        // DELETE api/values/5
        public IHttpActionResult Delete(int id)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    Supervisor supervisor = db.Supervisors.Find(id);
                    db.Supervisors.Remove(supervisor);
                    db.SaveChanges();
                    return Ok(id);
                }
                catch (Exception ex)
                {
                    return BadRequest();
                }
        }
    }
}
