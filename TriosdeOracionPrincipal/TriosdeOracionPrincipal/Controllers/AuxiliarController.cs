﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TriosdeOracionPrincipal.Controllers
{
    public class AuxiliarController : ApiController
    {
        public IHttpActionResult Get()
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    IEnumerable<Auxiliar> auxiliar = db.Auxiliars.ToList();
                    return Ok(auxiliar);
                }
                catch (Exception ex)
                {
                    return NotFound();
                }
        }

        // GET api/values/5
        public IHttpActionResult Get(int id)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    Auxiliar auxiliar = db.Auxiliars.Find(id);
                    return Ok(auxiliar);
                }
                catch (Exception ex)
                {
                    return NotFound();
                }
        }

        // POST api/values
        public IHttpActionResult Post([FromBody]Auxiliar value)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    db.Auxiliars.Add(value);
                    db.SaveChanges();
                    return Ok(value);
                }
                catch (Exception ex)
                {

                    return BadRequest();
                }

        }

        // PUT api/values/5
        public IHttpActionResult Put(int id, [FromBody]Auxiliar value)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    Auxiliar auxiliar = db.Auxiliars.Where(d => d.IdAuxiliar == id).First();
                    auxiliar.Nombre = value.Nombre;
                    auxiliar.Apellido = value.Apellido;
                    auxiliar.FecNac = value.FecNac;
                    auxiliar.EstadoCivil = value.EstadoCivil;
                    auxiliar.Sexo = value.Sexo;
                    db.Entry(auxiliar).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return Ok(auxiliar);
                }
                catch (Exception ex)
                {

                    return BadRequest();
                }
        }

        // DELETE api/values/5
        public IHttpActionResult Delete(int id)
        {
            using (TriosOracionEntities db = new TriosOracionEntities())
                try
                {
                    Auxiliar auxiliar = db.Auxiliars.Find(id);
                    db.Auxiliars.Remove(auxiliar);
                    db.SaveChanges();
                    return Ok(id);
                }
                catch (Exception ex)
                {
                    return BadRequest();
                }
        }
    }
}
